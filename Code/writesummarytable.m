function [ output_args ] = writesummarytable(fn, errorData, colHeaders)
%WRITESUMMARYTABLE Write summary table to CSV file

    % Summarize error counts
    [m n] = size(errorData);
    for i = 1:m
        for j = 1:n
            errors(i, j) = sum(errorData{i, j}.errorTypeI);
        end
    end
    
    % Open file
    fid = fopen(fn, 'w');
    
    % Write title
    fprintf(fid, 'SUMMARY TABLE\n');
    fprintf(fid, 'Error counts on six Bayesian classifers\n');
  
    % Write column headers
    fprintf(fid, 'Test set:,');
    for i = 1:length(colHeaders)
        fprintf(fid, '%s,', colHeaders(i));
    end
    fprintf(fid, '\nMethod\n');
    for i = 1:m
        fprintf(fid, '%d,', i);
        for j = 1:n
            fprintf(fid, '%d,', errors(i,j));
        end
        fprintf(fid, '\n');
    end
    
    % Close file
    fclose(fid);

end

