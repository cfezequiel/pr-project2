function [ c2 ] = classifier2( trainingSet, dataset )
%CLASSIFIER2 Bayes moment classifier with identical  covariances
%   
%   Input:
%       trainingSet - training set
%       dataset - sample dataset (i.e datasetB, datasetC, ...)
%   Output is a structure containing the following:
%       out.scores
%           NxM matrix consisting of the 'score' value for each class of
%           each sample
%           N - number of samples
%           M - number of classes
%           This is for the score table.
%       out.classes
%           Nx1 matrix consisisting of the classification for each sample

means = trainingSet.means;
aveCov = trainingSet.aveCov;
mah_dis = pdist2(dataset.ncm,means,'mahalanobis',aveCov);

k=1;
for n=1:100;

[min_dist(k), class(k)] = min(mah_dis(n,:));

k=k+1;
end;


c2.classifications= [class'];
c2.scores=[mah_dis];

