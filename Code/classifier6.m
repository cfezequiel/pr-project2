function [ out ] = classifier6(trainingSet, dataset)
%CLASSIFIER6 Bayes classifier in binary pixel space.
%   
%   Input:
%       trainingSet- training set class means
%       dataset - sample dataset (i.e datasetB, datasetC, ...)
%   Output is a structure containing the following:
%       out.scores
%           NxM matrix consisting of the 'score' value for each class of
%           each sample
%           N - number of samples
%           M - number of classes
%           This is for the score table.
%       out.classes
%           Nx1 matrix consisisting of the classification for each sample

means = trainingSet.means;
samples = dataset.bin;
nClasses = size(means, 1);
nSamples = size(samples, 1);

Prob = zeros(nSamples, nClasses);
for i = 1:nSamples
    x = samples(i, :);
    for j = 1:nClasses
        p = means(j, :);
        Prob(i, j) = prod((p .^ x) .* ((1 - p) .^ (1 - x)));
    end
end

class = zeros(1, nSamples);
for i = 1:nSamples
    [unused, class(i)] = max(Prob(i, :));
end

out.classifications = class';
out.scores = Prob;

end % function