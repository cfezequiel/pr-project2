function [ out ] = verifyclassifications(classifierData, classes)
%VERIFYCLASSIFICATIONS Verify output of a classifier
%   Input:
%       classifierData
%           Structure consisting of the following fields:
%           'classifications'
%               A M-by-1 matrix whose elements are the
%               the indices of the classes assigned to each of the M 
%               samples.
%           'scores'
%               A is a M-by-N matrix where each element corresponds to
%               the classifier score of a particular sample for one of the 
%               N classes
%
%       classes
%           A string of size N where each character represents one class.
%           
%   Output:
%       Structure consisting of:
%       'errorClassPerSample'
%           A M-by-1 matrix containing the class symbol (i.e. character)
%           assigned to each of the M samples.
%    	'classTally'
%           A N-by-N matrix whose element contains a counts of how many
%           samples are assigned to each class
%           Refer to the Confusion Table in the project specs.
%    	'errorTypeI'
%           A 1xN matrix containing Error Type I data for the Confusion
%           Table as defined in the project specs
%    	'errorTypeII'
%            A 1xN matrix containing Error Type II data for the Confusion
%           Table as defined in the project specs

    classifications = classifierData.classifications;
    
    nClasses = size(classes, 2);
    nSamples = size(classifications, 1);
    nSamplesPerClass = nSamples / nClasses;
    
    % Classification tally
    tally = zeros(nClasses, nClasses);
    errorTypeI = zeros(1, nClasses);
    errorTypeII = zeros(1, nClasses);
    
    for i = 1:nClasses
        iStart = (i - 1) * nSamplesPerClass + 1;
        iEnd = i * nSamplesPerClass;
        for j = iStart:iEnd
            c = classifications(j);
            if c ~= i
                errors(j) = c;
                tally(i, c) = tally(i, c) + 1;
                errorTypeI(i) = errorTypeI(i) + 1;
                errorTypeII(c) = errorTypeII(c) + 1;
            else
                errors(j) = 0;
                tally(i, i) = tally(i, i) + 1;
            end
        end
    end
    
    % Set output
    out.errorClassPerSample = errors;
    out.classTally = tally;
    out.errorTypeI = errorTypeI;
    out.errorTypeII = errorTypeII;
end

