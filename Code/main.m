% 
% CAP 6638 Pattern Recognition - Project 2
%
% 
% Authors:
%   Sinan Onal
%   Carlos Ezequiel
%

clear all;
clc;

% Set classifiers
classifiers = {@classifier1, @classifier2, @classifier3, @classifier4, ...
               @classifier5, @classifier6};         
nClassifiers = size(classifiers, 2);

% Extract features the sample datasets (A, B, C, D) respectively
pqIndices = [0 0; 0 2; 0 3; 1 1; 1 2; 2 0; 2 1; 3 0];
nDatasets = 4;
datasets = cell(nDatasets, 1);
datasets{1} = extractfeatures('data_setA.dat', pqIndices, []);
overallRms = datasets{1}.overallRms;
datasets{2} = extractfeatures('data_setB.dat', pqIndices, overallRms);
datasets{3} = extractfeatures('data_setC.dat', pqIndices, overallRms);
datasets{4} = extractfeatures('data_setD.dat', pqIndices, overallRms);

% Compute training set for classifiers 1-3
classes = 'acemnorsxz';
trainingSet13 = computetrainingsetncm(datasets{1}.ncm, classes);
trainingSet4 = computetrainingsetncm(datasets{1}.ncm(:, 1:4), classes);
trainingSet56 = computetrainingsetbin(datasets{1}.bin, classes);

% Run classifiers on all datasets and collect classification data
classifierData = cell(nClassifiers, nDatasets);
for i=1:nClassifiers 
    if i <= 3
        trainingSet = trainingSet13;
    elseif i == 4
        trainingSet = trainingSet4;
    else
        trainingSet = trainingSet56;
    end
    for j=1:nDatasets
        classifierData{i, j} = classifiers{i}(trainingSet, datasets{j});
    end
end

% Verify classifications and collect error data
errorData = cell(nClassifiers, nDatasets);
for i = 1:nClassifiers
    for j = 1:nDatasets
        errorData{i, j} = verifyclassifications(classifierData{i, j}, ...
                                                classes);
    end
end

% Write the summary table
outputDir = '../Output';
datasetNames = 'ABCD';
filename = sprintf('%s/summarytable.csv', outputDir);
writesummarytable(filename, errorData, datasetNames);

% Write the score tables (Dataset B only)
classifierNames = ...
    {'Method 1 - Eight Moments, Identity Covariance Matrix', ...
     'Method 2 - Eight Moments, Identical Covariance Matrices', ...
     'Method 3 - Eight Moments, Individual Class Covariances,' ...
     'Method 4 - Four Moments, Individual Class Covariances,' ...
     'Method 5 - Minimum Distance Classifier in Binary Pixel Space', ...
     'Method 6 - Bayes Classifier in Binary Pixel Space'};
for i = 1:nClassifiers
    filename = sprintf('%s/scoretable_B%d.csv', outputDir, i);
    title = sprintf('%s (trained on A, tested on B)', classifierNames{i});
    % Scale scores
    sDigits = 3;
    scores = classifierData{i, 2}.scores;
    
    % Scale all scores except for classifier 6's
    % FIXME: classifier 6 scores are INF when scaled
    if i ~= 6
        [scores factor] = scaletoint(scores, sDigits);
        writescoretable(filename, title, classes, scores, ...
                    errorData{i, 2}.errorClassPerSample);
    else
        % Score values for classifier 6 data have scientific notation
        writescoretable(filename, title, classes, scores, ...
                    errorData{i, 2}.errorClassPerSample, 'e');
    end
    
end

% Write the confusion tables (classifier 1 only)
for i = 1:nDatasets
    filename = sprintf('%s/conftable_%s1.csv', outputDir, datasetNames(i));
    title = sprintf('%s (trained on A, tested on %s)', ...
        classifierNames{1}, datasetNames(i));
    writeconfusiontable(filename, title, classes, errorData{1, i});
end

disp('Done.')