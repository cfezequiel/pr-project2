function [out] = centralmoment(iMean, jMean, p, q, x)

    out = 0;
    [n, m] = size(x);
    for i=1:n
        for j=1:m
            out = out + ((i - iMean) .^ p) .* ((j - jMean) .^ q) .* x(i,j);
        end
    end
    
end


