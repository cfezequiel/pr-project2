function [ out ] = overallrms(dataSet)
% OVERALLRMS Computes the overall root-mean-square (RMS) value for each 
% column of the data set.
% Returns a row vector of RMS values

    nSamples = size(dataSet, 1);
    out = sqrt(sum(dataSet .^ 2) ./ nSamples);

end

