function [ c3 ] = classifier3(trainingSet, dataset)
%CLASSIFIER3 Bayes moment classifier with individual class covariance
%   
%   Input:
%       trainingSet - training set
%       dataset - sample dataset (i.e datasetB, datasetC, ...)
%   Output is a structure containing the following:
%       out.scores
%           NxM matrix consisting of the 'score' value for each class of
%           each sample
%           N - number of samples
%           M - number of classes
%           This is for the score table.
%       out.classes
%           Nx1 matrix consisisting of the classification for each sample

nSamples = size(dataset.ncm, 1);
nClasses = size(trainingSet.means, 1);
classifications = zeros(1, nClasses);
scores = zeros(nSamples, nClasses);
g = zeros(1, nClasses);
for i = 1:nSamples
    for j = 1:nClasses
        u = trainingSet.means(j, :)';
        E = trainingSet.cov(:,:,j);
        W = -0.5.*inv(E);
        w = inv(E)*u;
        w0 = -0.5.*u'*inv(E)*u-0.5.*(log(det(E)));
        x = dataset.ncm(i,:)';
        g(j)= x'*W*x + w'*x + w0;
    end
    classifications(i) = find(g == max(g), 1, 'first');
    scores(i, :) = g;
end

% Set outputs
c3.classifications = classifications';
c3.scores = scores;

