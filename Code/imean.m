function [i_mean]= imean( X )

num_i=0;

for i=1:25;
    for j=1:35;
        num_i=num_i+(X(i,j)*i);
    end;
end

den= 0;
for i=1:25;
    for j=1:35;
        den=den+X(i,j);
    end;
end;

i_mean= num_i/den;
