function [ out ] = invdataset(dataSet)
%INVDATASET Get the inverse of all square matrices of a set of matrices
%   
    nSlices = size(dataSet, 3);
    for i=1:nSlices;
        out(:, :, i) = inv(dataSet(:,:,i));
    end
    
end

