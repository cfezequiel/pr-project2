function [ y factor ] = scaletoint(x, n)
%SCALETOINT Scale all values of an input matrix such that they are
%       integers have a minimum of n significant digits.

    vMin = min(min(x));
    vMax = max(max(x));
    f = 10^(ceil(-(log10(vMin))));
    k = ceil(log10(vMax*f));
    if k < n
        f = f * 10^(n - k);
    end
    y = round(x.*f);
    factor = f;
end

