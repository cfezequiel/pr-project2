function [out] = classifier5(trainingSet, dataset)
%CLASSIFIER5  Minimum distance classifier in binary pixel space
%   
%   Input:
%       trainingSet- training set class means
%       dataset - sample dataset (i.e datasetB, datasetC, ...)
%   Output is a structure containing the following:
%       out.scores
%           NxM matrix consisting of the 'score' value for each class of
%           each sample
%           N - number of samples
%           M - number of classes
%           This is for the score table.
%       out.classes
%           Nx1 matrix consisisting of the classification for each sample

means = trainingSet.means;
eucDist = pdist2(dataset.bin, means); %eucledian distance

nSamples = size(dataset.bin, 1);
for i = 1:nSamples
    [min_dist(i), class(i)] = min(eucDist(i,:));
end

out.classifications= [class'];
out.scores=[eucDist];

end

