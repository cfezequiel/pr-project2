function [ out ] = writescoretable(fn, title, classes, scores, errors, format)
%WRITESCORETABLE Write score table to CSV file
    
    if nargin < 6
        format = 'f';
    end
    
    % Open file
    fid = fopen(fn, 'w');
    
    % Write title
    fprintf(fid, '"%s"\n', title);
    
    % Write column headers
    nClasses = size(classes, 2);
    fprintf(fid, ',');
    for i = 1:nClasses
        fprintf(fid, '%s,', classes(i));
    end
    fprintf(fid, 'ERRORS\n');
       
    % Write row headers and scores
    [m n] = size(scores);
    for i = 1:m
        iClass = ceil(i/nClasses);
        fprintf(fid, '%s%d,', classes(iClass), mod(i-1, 10) + 1);
        for j = 1:n
            s = sprintf('%%%s,', format);
            fprintf(fid, s, scores(i,j));
        end
        iClass = errors(i);
        if iClass ~= 0
            fprintf(fid, '%s\n', classes(iClass));
        else
            fprintf(fid, '\n');
        end 
    end
    
    % Close file
    fclose(fid);
    
end

