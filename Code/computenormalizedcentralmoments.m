function [out orms] = computenormalizedcentralmoments(cmDataSet, overallRms)

    [nSamples, nMoments] = size(cmDataSet);
    if isempty(overallRms)
        orms = overallrms(cmDataSet);
    else
        orms = overallRms;
    end
    for i=1:nSamples;
        for j=1:nMoments;
            out(i,j)= cmDataSet(i,j)/orms(:, j);
        end
    end
    
end


