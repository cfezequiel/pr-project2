function [ c1 ] = classifier1( trainingSet,dataset)
%CLASSIFIER1 Minimum distance moment classifier 
%   
%   Input:
%       trainingSet- training set class means
%       dataset - sample dataset (i.e datasetB, datasetC, ...)
%   Output is a structure containing the following:
%       out.scores
%           NxM matrix consisting of the 'score' value for each class of
%           each sample
%           N - number of samples
%           M - number of classes
%           This is for the score table.
%       out.classes
%           Nx1 matrix consisisting of the classification for each sample

means = trainingSet.means;
euc_dis = pdist2(dataset.ncm,means); %eucledian distance

k=1;
for n=1:100;

[min_dist(k), class(k)] = min(euc_dis(n,:));

k=k+1;
end;

c1.classifications= [class'];
c1.scores=[euc_dis];


 
