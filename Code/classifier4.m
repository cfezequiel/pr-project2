function [ c4 ] = classifier4(trainingSet, dataset)
%CLASSIFIER4 Bayes moment classifier with individual class covariance
%   
%   Input:
%       trainingSet - training set
%       dataset - sample dataset (i.e datasetB, datasetC, ...)
%   Output is a structure containing the following:
%       out.scores
%           NxM matrix consisting of the 'score' value for each class of
%           each sample
%           N - number of samples
%           M - number of classes
%           This is for the score table.
%       out.classes
%           Nx1 matrix consisisting of the classification for each sample

% Get first four features of dataset for each sample
ds = dataset;
ds.ncm = ds.ncm(:, 1:4);

% Get first four class means and covariances for each training set class
ts = trainingSet;
ts.means = ts.means(:, 1:4);
ts.cov = ts.cov(:, 1:4, :);

% Run the classifier
c4 = classifier3(ts, ds);


