function [ out ] = computecentralmoments(dataSet, pqIndices)
%COMPUTECENTRALMOMENTS Compute the central moments for a given dataset
%   
    nIndices = size(pqIndices, 1);
    nSamples = size(dataSet, 3);
    for iCol = 1:nIndices;
        p = pqIndices(iCol,1);
        q = pqIndices(iCol,2);
        for iRow = 1:nSamples;
            x = dataSet(:, :, iRow);
            iMeanX = imean(x);
            jMeanX = jmean(x);
            out(iRow, iCol) = centralmoment(iMeanX, jMeanX, p, q, x);
        end
    end

end

