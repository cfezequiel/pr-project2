function [ out ] = computetrainingsetbin(binFeatures, classes)
%COMPUTETRAININGSETBIN Compute the training set using binary image data

    nClasses = size(classes, 2);
    
    % Partition the binary feature set depending on the number of classes
    [nSamples, sampleSize] = size(binFeatures);
    nSamplesPerClass = nSamples / nClasses;
    binClassMeans = zeros(nClasses, sampleSize); 
    for i = 1:nClasses
        b = (i - 1) * nSamplesPerClass + 1;
    	e = i * nSamplesPerClass;
        binClassMeans(i, :) = (sum(binFeatures(b:e, :)) + 1) / ...
            (nSamplesPerClass + 2); 
    end
    
    out.means = binClassMeans;

end

