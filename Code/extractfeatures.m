function [ out ] = extractfeatures(filename, pqIndices, overallRms)
%EXTRACTFEATURES Extract features from input dataset file.

    % Import raw data from file
    rawData = importdata(filename);

    % Parse raw data into a set of matrices
    dataSet = splitrows(rawData, 25);

    % 'Flip' each matrix of the data set
    dataSet = flip(dataSet);

    % Compute central moments for each sample
    cm = computecentralmoments(dataSet, pqIndices);
    
    % Normalize the central moments for each sample
    [ncm orms] = computenormalizedcentralmoments(cm, overallRms);
    
    % Convert binary image matrices to set of vectors
    for i = 1:size(dataSet, 3)
        bin(i, :) = reshape(dataSet(:, :, i).', 1, []);
    end
    
    % Set outputs
    out.ncm = ncm;
    out.bin = bin;
    out.overallRms = orms;
    
end

